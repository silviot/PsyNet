![Logo](https://gitlab.com/PsyNetDev/PsyNet/-/raw/master/psynet/resources/logo.svg)

PsyNet is a powerful new Python package for designing and running the next generation of online behavioural experiments.
It builds on the virtual lab framework [Dallinger](https://dallinger.readthedocs.io/)
to streamline the development of highly complex experiment paradigms, ranging from simulated cultural evolution to
perceptual prior estimation to adaptive psychophysical experiments. Once an experiment is implemented, it can be
deployed with a single terminal command, which looks after server provisioning, participant recruitment, data-quality
monitoring, and participant payment. Researchers using PsyNet can enjoy a paradigm shift in productivity, running many
high-powered variants of the same experiment in the time it would ordinarily take to run an experiment once.

To try some real-world PsyNet experiments for yourself, visit the following repositories:

- [Consonance profiles for carillon bells](https://github.com/pmcharrison/2022-consonance-carillon)
- [Emotional connotations of musical scales](https://github.com/pmcharrison/2022-musical-scales)
- [Vocal pitch matching in musical chords](https://github.com/pmcharrison/2022-vertical-processing-test)

For more information about PsyNet, visit the [documentation website](https://psynetdev.gitlab.io/PsyNet/).
