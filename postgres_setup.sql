CREATE DATABASE dallinger;
CREATE DATABASE "dallinger-import";
GRANT ALL PRIVILEGES ON DATABASE dallinger TO dallinger;
GRANT ALL PRIVILEGES ON DATABASE "dallinger-import" TO dallinger;