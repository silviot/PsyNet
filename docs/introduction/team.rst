.. _team:

Team
====

The PsyNet team is distributed over several academic institutions,
including primarily the Max Planck Institute for Empirical Aesthetics
and the University of Cambridge.

**Core team**

- `Peter Harrison <https://cms.mus.cam.ac.uk/staff/dr-peter-harrison>`_: Project creator, co-director, developer, and maintainer
- Frank Höger: Project development and maintenance
- `Pol van Rijn <https://pol.works/>`_: Project development and maintenance
- `Nori Jacoby <https://norijacoby.com/>`_: Project co-director

**Additional contributors (A-Z)**

- `Fotini Deligiannaki <https://gr.linkedin.com/in/fotini-deligiannaki-8b660621b>`_
- `Harin Lee <https://www.cbs.mpg.de/employees/hlee>`_
- `Manuel Anglada-Tort <https://www.manuelangladatort.com/>`_
- `Ofer Tchernichovski <https://www.hunter.cuny.edu/psychology/people/faculty/physiological/tchernichovski>`_
- `Raja Marjieh <https://il.linkedin.com/in/raja-marjieh-505b0781>`_

We also collaborate regularly with the Dallinger initiative,
an open-source software project for network-based experiments initially developed by
`Jordan Suchow <https://suchow.io/>`_,
`Tom Morgan <https://shesc.asu.edu/people/thomas-morgan>`_,
and `Tom Griffiths <https://cocosci.princeton.edu/tom/index.php>`_
and maintained by the software consultancy
`Jazkarta <https://www.jazkarta.com/>`_.
