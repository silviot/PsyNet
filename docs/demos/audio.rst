=====
Audio
=====

This demo illustrates an eclectic variety of audio interfaces in PsyNet.
These include both ways to produce and to record sound.


Source: ``demos/audio``

.. literalinclude:: ../../demos/audio/experiment.py
   :language: python