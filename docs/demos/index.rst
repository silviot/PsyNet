Demos
=====

.. toctree::
   :maxdepth: 1

   introduction
   hello_world
   timeline
   survey_js
   trial
   trial_2
   trial_3
   audio
   static_audio
   gibbs
   audio_gibbs
   imitation_chains
   tapping_imitation_chain
   mcmcp

