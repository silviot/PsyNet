=================
Audio trial maker
=================

This demo illustrates the use of a trial maker in an audio experiment.
Trial makers provide a standardized way to administer trials to the participant
that can save you some effort compared to implementing the required logic manually.

These trials involve recording audio from the participant. This is achieved using the
``AudioRecordControl`` class.


Source: ``demos/static_audio``

.. literalinclude:: ../../demos/static_audio/experiment.py
   :language: python
