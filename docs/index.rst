Overview
========

PsyNet is a new platform for running advanced behavioral experiments
ranging from adaptive psychophysics to simulated cultural evolution.
It builds on the virtual lab framework `Dallinger <https://dallinger.readthedocs.io/en/latest/>`_.
Its goal is to enable researchers to implement and deploy experiments as efficiently as possible,
while placing minimal constraints on the complexity of the experiment design.

This website contains a variety of resources to help you learn more about PsyNet.
Some particularly useful resources are highlighted below,
but see the sidebar for a full list.

- :ref:`When to use PsyNet? <applications>`: Learn about the use cases for which PsyNet is optimized.

- :ref:`Demos <demos_introduction>`: See demos of different PsyNet features.

- :ref:`Example experiments <example_experiments>`: See code repositories for real-world PsyNet experiments.

- `GitLab repository <https://gitlab.com/PsyNetDev/PsyNet>`_: Explore PsyNet's source code.


.. toctree::
   :maxdepth: 1
   :hidden:
   :caption: Introduction

   self
   introduction/applications
   introduction/history
   introduction/team
   introduction/command_line

.. toctree::
   :maxdepth: 1
   :hidden:
   :caption: Learning

   learning/how_to_learn
   learning/prerequisites
   learning/exercises/index
   learning/tracks/index

.. toctree::
   :maxdepth: 1
   :hidden:
   :caption: Dependencies

   dependencies/dallinger
   dependencies/docker

.. toctree::
   :hidden:
   :caption: INSTALLATION

   installation/index

.. toctree::
   :hidden:
   :caption: EXPERIMENT DEVELOPMENT

   experiment_development/experiment_directory
   experiment_development/development_workflow
   experiment_development/configuration
   experiment_development/dependencies
   experiment_development/troubleshooting
   demos/index

.. toctree::
   :hidden:

   example_experiments/index

.. toctree::
   :hidden:

   tutorials/index

.. toctree::
   :hidden:

   api/index

.. toctree::
   :maxdepth: 1
   :hidden:
   :caption: DEPLOYING

   deploy/web_servers
   deploy/aws_server_setup
   deploy/ssh_server
   deploy/heroku_server
   deploy/prolific
   deploy/deploy_from_archive
   deploy/export
   deploy/troubleshooting

.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: Contributing to PsyNet
   :glob:

   developer/workflow
   developer/contributing_a_feature_or_bugfix
   developer/prescreening_tasks
   developer/updating_documentation
   developer/internationalization
   developer/running_tests
   developer/making_a_release

.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: Dashboards
   :glob:

   dashboards/translation

   
