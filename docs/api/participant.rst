===========
Participant
===========

.. autofunction:: psynet.participant.get_participant

.. autoclass:: psynet.participant.Participant
    :members:
    :exclude-members: id
