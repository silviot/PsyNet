#####
Trial
#####

.. toctree::
   :maxdepth: 3
   :caption: Trial
   :glob:

   audio
   audio_gibbs
   chain
   dense
   gibbs
   graph
   imitation_chain
   main
   media_gibbs
   mcmcp
   record
   static
   video
