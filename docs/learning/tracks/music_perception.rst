======================
Music perception track
======================

.. warning::
    This part of the PsyNet documentation is still work in progress.

The music perception track is tailored towards people who want to run online behavioral studies
about how people perceive music.

The first step is to `install PsyNet <../../installation/index.html>`_.
You should make sure you do the optional step 5, namely installing editable PsyNet and Dallinger repositories.

Next you should read the
`introduction to the demos section <../../demos/introduction.html>`_,
and then explore the first two demos,
`Hello world <../../demos/hello_world.html>`_,
and `Timeline <../../demos/timeline.html>`__.
These demos introduce you to the basics of PsyNet experiments.
You should approach each demo in the following way:

- Copy and paste the demo from the PsyNet source code location into another location on your computer.
- Open the demo as a new PyCharm project.
- Run the demo following the standard approach (``psynet local debug``, or ``docker/psynet local debug``).
- Read the source code in the demo and relate it to the behavior of the demo.
- Try making some changes to the demo and see how they change the experiment. Note that minor changes will be
  reflected if you just save the code and refresh the page, but major changes (e.g. adding pages) may require
  you to restart the experiment (hit CTRL-C to stop the experiment, then rerun the original command to relaunch it).

Now take the following tutorials:

- `Classes in PsyNet <../../tutorials/classes.html>`_
- `Timeline <../../tutorials/timeline.html>`_
- `Modular pages <../../tutorials/modular_page.html>`_

Take the `timeline exercise <../exercises/timeline.html>`__.

Explore the `audio demo <../../demos/audio.html>`_, then take the `JSSynth exercises <../exercises/js_synth.html>`_.

Explore the following trial demos:

- `Trial (1) <../../demos/trial.html>`_
- `Trial (2) <../../demos/trial_2.html>`_
- `Trial (3) <../../demos/trial_3.html>`_
- `Audio trial maker <../../demos/static_audio.html>`_
