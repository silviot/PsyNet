=================
JSSynth exercises
=================

Prerequisites
^^^^^^^^^^^^^

- `Timeline tutorial <../../tutorials/timeline.html>`_
- `Modular page tutorial <../../tutorials/modular_page.html>`_
- `Audio demo <../../demos/audio.html>`_

Exercise 1
^^^^^^^^^^

Create a modular page that plays an ascending arpeggio beginning on middle C (MIDI 60).

Exercise 2
^^^^^^^^^^

Create an experiment that asks the participant whether they want to hear a major arpeggio or a minor arpeggio,
and plays them that arpeggio.

**Tip**: Your question should go on a separate page to your music player.

Exercise 3
^^^^^^^^^^

Create an modular page that plays a random sequence of tones drawn from the chromatic scale.

Exercise 4
^^^^^^^^^^

Extend your result from Exercise 3 to solicit a 'preference' rating from the participant on a scale from 1-4.
