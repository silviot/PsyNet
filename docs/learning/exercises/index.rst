Exercises
=========

Here we provide a miscellaneous collection of exercises that introduce you to various parts of PsyNet.
For curated collections of exercises see `Tracks <../tracks/index.html>`_.

.. warning::
    This part of the PsyNet documentation is still work in progress.

.. toctree::
    :maxdepth: 1

    timeline
    graphics
    js_synth
