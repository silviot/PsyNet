The first step is to identify which project you want to set up.
A PsyNet project is identified with an 'experiment directory', which is a folder
containing various standardized files defining an experiment implementation,
most importantly an ``experiment.py`` file and a ``requirements.txt`` file.
If you are exploring PsyNet for the first time, we recommend that you start with
one of the demo experiments, which can be found in the ``demos`` folder of the PsyNet repository.
Alternatively, you can download one of the example experiments listed on the PsyNet website.
If you are planning to create your own experiment, you can start by copying one of the demo experiments
to a new directory and modifying it as appropriate.
