Installing PsyNet via virtualenv (Windows)
==============================================

Installing PsyNet on a Windows machine depends on the “Windows Subsystem for Linux” (WSL).
All code you run using your the installation needs to be run within the Linux subsystem.

Step 0: Install WSL
^^^^^^^^^^^^^^^^^^^

.. include:: ../wsl_installation.rst

Once you've installed WSL, you probably will need to restart your computer before continuing.
Then open your Ubuntu terminal and follow the below instructions:

Step 1: Perform Linux  installation
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. include:: linux_dev_installation.rst

Troubleshooting
^^^^^^^^^^^^^^^

.. include:: ../wsl_troubleshooting.rst
