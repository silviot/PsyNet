Installing PsyNet via Docker
============================

.. toctree::
    :maxdepth: 1

    unix_installation
    windows_installation
