Installing PsyNet via Docker (macOS/Linux)
==========================================

.. include:: shared_introduction.rst

.. include:: shared_installation.rst
