Installation overview
=====================

There are two main ways to install PsyNet: the *virtualenv* route and the *Docker* route.
We generally recommend the ``virtualenv`` route for most users:
the installation process includes a few more steps but the end result is a more flexible installation.
Follow the links below for instructions.

.. toctree::
    :maxdepth: 1

    virtualenv_installation/index
    docker_installation/index


If you are interested in contributing to PsyNet, you should also complete
the `Additional developer installation steps <additional_developer_installation.html>`_.
