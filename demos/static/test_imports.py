# This file is here to support PsyNet's regression tests


def my_add(a, b):
    return a + b


class MyClass:
    def __init__(self, x):
        self.x = x

    def add(self, y):
        return self.x + y
