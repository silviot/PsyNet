# pylint: disable=unused-import,abstract-method

import logging

import psynet.experiment
from psynet.consent import NoConsent
from psynet.page import InfoPage, SuccessfulEndPage
from psynet.timeline import CodeBlock, Module, PageMaker, Timeline, for_loop

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger()


def check_module_b(participant):
    assert not participant.locals.has("animal")
    assert participant.locals.color == "blue"
    assert participant.module_states["module_a"][0].var.animal == "cat"

    export = participant.to_dict()
    assert export["module_a__animal"] == "cat"
    assert export["module_a__1__animal"] == "dog"


class Exp(psynet.experiment.Experiment):
    label = "Module demo"
    initial_recruitment_size = 1

    timeline = Timeline(
        NoConsent(),
        for_loop(
            label="module_a_loop",
            iterate_over=lambda: ["cat", "dog"],
            logic=lambda animal: Module(
                "module_a",
                CodeBlock(lambda participant: participant.locals.set("animal", animal)),
                PageMaker(
                    lambda participant: InfoPage(
                        f"Animal = {participant.locals.animal}",
                    ),
                    time_estimate=5,
                ),
            ),
            time_estimate_per_iteration=5,
        ),
        Module(
            "module_b",
            CodeBlock(lambda participant: participant.locals.set("color", "blue")),
            PageMaker(
                lambda participant: InfoPage(
                    f"Color = {participant.locals.color}",
                ),
                time_estimate=5,
            ),
            CodeBlock(check_module_b),
        ),
        SuccessfulEndPage(),
    )
